var fs = require( "fs" );

var file = `var x = "%%LETTER%%";

export var result = \`%%NUMBER%% = \${x}\`;`;
var big = "";
const FILES = 63;

function getLetter( number ){
    var singles = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_";

    return singles.split( "" )[ number ];
}

for( let i = 1; i <= FILES; i++ ){
    let letter = getLetter( i );
    let fileContent = file.replace( "%%LETTER%%", letter ).replace( "%%NUMBER%%", i );
    fs.writeFileSync( `${i}.js`, fileContent, "utf8" );
    big += `
import { result as result${i} } from "./${i}.js";`
}

big += "\n\nvar results = [";
for( let i = 1; i <= FILES; i++ ){
    big += `result${i},
`;
}
big += `];

results.forEach( ( result ) => {
    console.log( result );
} );`;

fs.writeFileSync( "test.js", big, "utf8" );